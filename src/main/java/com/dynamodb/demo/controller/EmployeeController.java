package com.dynamodb.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dynamodb.demo.entity.Employee;
import com.dynamodb.demo.repository.EmployeeRepo;

@RestController
public class EmployeeController {
	
	@Autowired
	private EmployeeRepo employeeRepo;
	
	@PostMapping("/employee")
	public Employee saveEmployee(@RequestBody Employee employee) {
	return employeeRepo.save(employee);
	}
	
	@GetMapping("/employee/{id}")
	public Employee getEmployee(@PathVariable("id") String employeeId) {
		return employeeRepo.getEmployeeById(employeeId);
	}
	
	@DeleteMapping("/employee/{id}")
	public String deleteEmployee(@PathVariable("id") String employeeId) {
		return employeeRepo.delete(employeeId);
	}
	
	@PutMapping("/employee/update")
	public String updateEmployee(@RequestBody Employee employee) {
		return employeeRepo.updateEmployee(employee);
	}
}
